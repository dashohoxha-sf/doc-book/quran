<?php
  /*
   This file is part of DocBookWiki.  DocBookWiki is a web application
   that displays and edits DocBook documents.

   Copyright (C) 2004, 2005, 2006, 2007
   Dashamir Hoxha, dashohoxha@users.sourceforge.net

   DocBookWiki is free software; you can redistribute it and/or modify
   it under the  terms of the GNU General  Public License as published
   by the Free  Software Foundation; either version 2  of the License,
   or (at your option) any later version.

   DocBookWiki is distributed in the  hope that it will be useful, but
   WITHOUT  ANY  WARRANTY;  without   even  the  implied  warranty  of
   MERCHANTABILITY or  FITNESS FOR A PARTICULAR PURPOSE.   See the GNU
   General Public License for more details.

   You should have  received a copy of the  GNU General Public License
   along  with  DocBookWiki;  if  not,  write  to  the  Free  Software
   Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307
   USA
  */

  /** 
   * Upload the files 'xml_file' and 'media_files'.
   * Sets the global variables $fnames (files that are being uploaded)
   * and $upload_msg (successfullnes of upload), which are used to give
   * a feedback message to the user.
   */
function upload_files()
{
  global $fnames, $upload_msg;

  //get the xml_file
  $tmp_name = $_FILES['xml_file']['tmp_name'];
  $file_name = $_FILES['xml_file']['name'];
  $xml_file = basename($file_name);
  $dest_file = "content/initial_xml/uploaded/$xml_file";
  $msg = check_upload_status('xml_file');
  if ($msg=='')  $msg = move_file($tmp_name, $dest_file);

  //set the global variables
  $fnames = $xml_file;
  $upload_msg = $msg;

  //if there is no media_files, return
  $file_name = $_FILES['media_files']['name'];
  if ($file_name=='')  return;

  //get the media_files
  //the destination file has the same basename as the xml_file,
  //but with extension .media.tgz instead of .xml
  $tmp_name = $_FILES['media_files']['tmp_name'];
  $media_files = ereg_replace('\.xml$', '.media.tgz', $xml_file);
  $dest_file = "content/initial_xml/uploaded/$media_files";
  $msg = check_upload_status('media_files');
  if ($msg=='')  $msg = move_file($tmp_name, $dest_file);

  //update the global variables
  $fnames .= ', '.$media_files;
  $upload_msg .= "<br/>\n".$msg;
}
?>