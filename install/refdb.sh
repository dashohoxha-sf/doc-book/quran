#!/bin/bash
### create the refdb user that is used by docbookwiki

### go to the root dir
cd $(dirname $0)/..

### get the refdb settings from 'books.conf'
. books.conf
HOST=$REFDB_HOST
USER=$REFDB_USER
PASS=$REFDB_PASS
DATABASE=$REFDB_DATABASE

### get username and password of the admin of the refdb server
### it is actually the admin of the mysql server (usually 'root')
#read -p "Enter username of the admin of refdb server [root]: " server_admin
#server_admin=${server_admin:-root}
#read -p "Enter password of the admin of refdb server []: " admin_passwd
server_admin=root
admin_passwd=

### add a new user
refdba -i $HOST -u $server_admin -w "$admin_passwd" \
       -C adduser -d $DATABASE -H $HOST -W $PASS $USER

