#!/bin/bash

### go to the root directory
cd $(dirname $0)
cd ..

### configure the application files
install/configure.sh

### make the translation files
l10n/msgfmt-all.sh

### set 'admin' as the default superuser password
install/set_su_passwd.sh admin

### prepare the database for webnotes
install/webnotes.sh

### add a user for docbookwiki in refdb
install/refdb.sh

### generate the content files from initial xml files
content/clean.sh all
content/SVN/init.sh
content/make-content.sh

### index the content for searching
search/make_index.sh

### run make-all-downloads.sh
content/downloads/checkout_xml_sources.sh
nice content/downloads/make-all-downloads.sh

### enable email notifications on commit
content/SVN/email-notifications.sh on
