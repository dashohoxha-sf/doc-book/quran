#!/bin/bash

xml_file=../../../content/books/xml/linux_server_admin/en/index.xml
node_path=./basicservices/email/
xsltproc --stringparam path $node_path ../move_up.xsl $xml_file \
  | grep basicservices
