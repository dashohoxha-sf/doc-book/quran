#!/bin/bash

xml_file=../../../content/books/xml/linux_server_admin/en/index.xml
node_path=./basicservices/network/
label=test
type=section
xsltproc --stringparam path "$node_path" \
         --stringparam id "$label" \
         --stringparam type "$type" \
         ../add_subnode.xsl $xml_file
