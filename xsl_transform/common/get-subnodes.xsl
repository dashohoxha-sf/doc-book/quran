<?xml version='1.0'?><!-- -*-SGML-*- -->
<!--
This file  is part of  DocBookWiki.  DocBookWiki is a  web application
that  displays  and  edits  DocBook  documents.  

Copyright (C) 2004, 2005 Dashamir Hoxha, dashohoxha@users.sf.net

DocBookWiki is free software; you can redistribute it and/or modify it
under the terms of the GNU  General Public License as published by the
Free Software Foundation; either version 2 of the License, or (at your
option) any later version.

DocBookWiki is  distributed in  the hope that  it will be  useful, but
WITHOUT   ANY  WARRANTY;   without  even   the  implied   warranty  of
MERCHANTABILITY  or FITNESS  FOR A  PARTICULAR PURPOSE.   See  the GNU
General Public License for more details.

You  should have received  a copy  of the  GNU General  Public License
along with DocBookWiki; if not, write to the Free Software Foundation,
Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
-->

<!--
Contains the template 'get-subnodes' which creates an html list of
the subnodes of the current node.  It is called with the parameter
book_dir, like this:
    <xsl:call-template name="get-subnodes">
      <xsl:with-param name="book_dir" select="$book_dir" />
    </xsl:call-template>
-->

<xsl:transform xmlns:xsl="http://www.w3.org/1999/XSL/Transform" version="1.0">

<xsl:include href="section-level.xsl" />
<xsl:include href="get-title.xsl" />

<!-- creates an html list of subnodes of the current node -->
<xsl:template name="get-subnodes">
  <xsl:param name="book_dir" />
  <xsl:choose>
    <xsl:when test="./simplesect">
      <ol type="1">
        <xsl:apply-templates mode="get-subnodes" />
      </ol>
    </xsl:when>
    <xsl:otherwise>
      <xsl:variable name="css_class">
      <xsl:apply-templates select="." mode="css_class" />
      </xsl:variable> 
      <dl class="{$css_class}">
        <xsl:apply-templates mode="get-subnodes" />
      </dl>
    </xsl:otherwise>
  </xsl:choose>
</xsl:template>

<!-- subnodes for a section -->
<xsl:template mode="get-subnodes"
              match="preface | appendix | bibliography | chapter | section">
 
  <xsl:variable name="href">
    <xsl:text>javascript:set_node('</xsl:text>
    <xsl:value-of select="@path" />
    <xsl:text>')</xsl:text>
  </xsl:variable>

  <xsl:variable name="css_class">
    <xsl:apply-templates select="." mode="css_class" />
  </xsl:variable> 

  <xsl:variable name="title">
    <xsl:call-template name="get-title">
      <xsl:with-param name="book_dir" select="$book_dir" />
      <xsl:with-param name="path" select="@path" />
    </xsl:call-template>
  </xsl:variable> 

  <xsl:variable name="dt_item">
    <xsl:choose>
      <xsl:when test="name(.)='preface'">
 	<xsl:value-of select="$title" />
      </xsl:when>
      <xsl:when test="name(.)='appendix'">
	<xsl:variable name="cnt">
	  <xsl:number count="appendix" from="/" level="single" format="A" />
	</xsl:variable>    
	<xsl:value-of select="concat($cnt, '. ', $title)" />
      </xsl:when>
      <xsl:when test="name(.)='bibliography'">
 	<xsl:value-of select="$title" />
      </xsl:when>
      <xsl:when test="name(..)='book' or name(..)='article'">
	<xsl:variable name="nr">
	  <xsl:number count="chapter | section"
                      from="/" level="single" format="1" />
	</xsl:variable>    
	<xsl:value-of select="concat($nr, '. ', $title)" />
      </xsl:when>
      <xsl:otherwise>
	<xsl:value-of select="$title" />
      </xsl:otherwise>
    </xsl:choose>
  </xsl:variable>

  <dt class="{$css_class}">
    <a href="{$href}"><xsl:value-of select="$dt_item"/></a>
  </dt>
  <xsl:if test="(name(..)='book' or name(..)='article' 
               or name(..)='preface' or name(..)='appendix' 
               or name(..)='chapter') and not(./simplesect)">
    <dd><dl class="{$css_class}">
        <xsl:apply-templates mode="get-subnodes"/>
    </dl></dd>
  </xsl:if>
</xsl:template>

<xsl:template mode="css_class"
              match="book | article | preface | appendix 
                    | bibliography | chapter">
  <xsl:value-of select="name(.)" />
</xsl:template>

<xsl:template mode="css_class" match="section">
  <xsl:variable name="level">
    <xsl:call-template name="get-section-level" />
  </xsl:variable>
  <xsl:value-of select="concat('sect', $level)" />
</xsl:template>

<!-- handle simplesect differently -->
<xsl:template match="simplesect" mode="get-subnodes">
  <xsl:variable name="count">
    <xsl:number count="simplesect" format="1" />
  </xsl:variable> 

  <xsl:variable name="type">
    <xsl:choose>
      <xsl:when test="$count mod 2 = 0">1</xsl:when>
      <xsl:otherwise>2</xsl:otherwise>
    </xsl:choose>
  </xsl:variable> 

  <xsl:variable name="href">
    <xsl:text>javascript:set_node('</xsl:text>
    <xsl:value-of select="@path" />
    <xsl:text>')</xsl:text>
  </xsl:variable> 

  <xsl:variable name="title">
    <xsl:call-template name="get-title">
      <xsl:with-param name="book_dir" select="$book_dir" />
      <xsl:with-param name="path" select="@path" />
    </xsl:call-template>
  </xsl:variable> 

  <xsl:variable name="summary">
    <xsl:variable name="xml-file">
      <xsl:value-of select="concat($book_dir, @path)" />
      <xsl:value-of select="'content.xml'" />
    </xsl:variable>
    <xsl:apply-templates select="document($xml-file)" mode="get-summary" />
  </xsl:variable> 

  <li value="{$title}" class="simplesect-li-{$type}">
    <xsl:copy-of select="$summary" />
    <a class="button" href="{$href}">-&gt;</a>
  </li>
</xsl:template>


<!-- ignore text nodes (get-subnodes) -->
<xsl:template match="text()" mode="get-subnodes" />


<!-- simplesect (get-summary) -->
<xsl:template match="simplesect" mode="get-summary">
  <xsl:apply-templates mode="get-summary" />
</xsl:template>


<!-- don't include the title in the summary of simplesect -->
<xsl:template match="simplesect/title" mode="get-summary" />


<!-- xref (get-summary) -->
<xsl:template mode="get-summary" match="xref">
<a class="xref" href="javascript:set_node_id('{@linkend}')">
  <xsl:text> [*] </xsl:text>
</a>
</xsl:template>


<!-- text (get-summary) -->
<xsl:template match="text()" mode="get-summary">
  <xsl:value-of select="normalize-space(.)" />
</xsl:template>


</xsl:transform>
