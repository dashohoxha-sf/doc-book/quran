<?xml version='1.0'?><!-- -*-SGML-*- -->
<!--
This file  is part of  DocBookWiki.  DocBookWiki is a  web application
that  displays  and  edits  DocBook  documents.  

Copyright (C) 2004, 2005 Dashamir Hoxha, dashohoxha@users.sf.net

DocBookWiki is free software; you can redistribute it and/or modify it
under the terms of the GNU  General Public License as published by the
Free Software Foundation; either version 2 of the License, or (at your
option) any later version.

DocBookWiki is  distributed in  the hope that  it will be  useful, but
WITHOUT   ANY  WARRANTY;   without  even   the  implied   warranty  of
MERCHANTABILITY  or FITNESS  FOR A  PARTICULAR PURPOSE.   See  the GNU
General Public License for more details.

You  should have received  a copy  of the  GNU General  Public License
along with DocBookWiki; if not, write to the Free Software Foundation,
Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
-->

<!--
Process the XML chunk 'content.xml' of the document and generate the
cache file 'content.html'.
It is included by 'cache/content-html.xsl' and 'cache/update_content.xsl'.
-->

<xsl:transform xmlns:xsl="http://www.w3.org/1999/XSL/Transform" version="1.0">

<!-- import the DocBook stylesheets used by `xmlto` -->
<xsl:import href="/usr/share/sgml/docbook/xsl-stylesheets/xhtml/onechunk.xsl"/>

<!-- import the DocBook stylesheets used by RefDB  -->
<!-- (used for converting <bibliography> to XHTML) -->
<xsl:import href="/usr/local/share/refdb/xsl/docbk-refdb-xsl/docbk-xhtml/docbk-refdb-xhtml.xsl" />

<!-- import the customizations that are made by DocBookWiki -->
<!-- to the standard DocBook stylesheets                    -->
<xsl:import href="./content.xsl" />

<!-- include any local customizations to DocBookWiki -->
<xsl:include href="../../local/content.xsl" />

</xsl:transform>
