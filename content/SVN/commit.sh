#!/bin/bash
### commit in svn the modifications done in a book

### go to this dir
cd $(dirname $0)

if [ "$1" = "" ]
then
  echo "Usage: $0 book_id [lng]"
  echo "where lng is en, de, fr, it, al, etc. (default is en)"
  exit 1;
fi

book_id=$1
lng=${2:-en}

### read the name of the svn sandbox from svn_cfg.txt
. svn_cfg.txt

xml_file=$svn_dir/${book_id}_${lng}.xml

### implode xml file
../implode/implode.sh $book_id $lng
mv ../implode/tmp/$book_id.xml $xml_file

### commit xml file
echo "Commiting '$xml_file'"
svn commit $xml_file -m \""$(hostname -i):$(pwd)"\"

### get media files
../implode/get_media.sh $book_id $lng
rm -rf tmp_media/
mkdir tmp_media/
if [ -f ../implode/tmp/$book_id.media.tgz ]
then
  tar xfz ../implode/tmp/$book_id.media.tgz -C tmp_media/
  rm -f ../implode/tmp/$book_id.media.tgz
fi

#### commit media files
#media_dir="$svn_dir/media/$book_id/$lng"    # svn_dir/media/book_id/lng
#echo "Commiting '$media_dir'"
#./svn_load_dirs.pl -wc $media_dir \
#                   $repository/trunk/ media/$book_id/$lng/  tmp_media/
rm -rf tmp_media/
