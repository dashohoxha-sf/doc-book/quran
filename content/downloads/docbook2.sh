#!/bin/bash
### Convert to other formats using docbook2html, docbook2pdf, etc.
### (SGML docbook + DSSSL stylesheets).
### Has parameters: book_id, format, lng.
### The input file is 'formats/$book_id/$lng/xml/$book_id.$lng.xml'.
### Media files of the book should be in: 'formats/$book_id/$lng/media/'.
### The output is placed in directory: 'formats/$book_id/$lng/$format/'.

### go to this dir
cd $(dirname $0)

function usage
{
  echo "Usage: ${0} book_id [ html | rtf | pdf | ps | tex | txt ] [lng]"
  exit 1
}

if [ "$1" = "" ]; then usage; fi
echo "--> $0 $1 $2 $3"

### get parameters
book_id=$1
format=$2
lng=${3:-en}

### set variables
book=$book_id.$lng
book_xml=formats/$book_id/$lng/xml/$book.xml
media=formats/$book_id/$lng/media
output=formats/$book_id/$lng/$format
tmp=formats/tmp
xml_file=$tmp/$book.docbook2.xml
stylesheets=/usr/share/sgml/docbook/dsssl-stylesheets

### make directories
mkdir -p $output/
mkdir -p $tmp/

### make a small modification in DOCTYPE declaration, remove the URL of DTD
### copy the xml file to a temporary input dir
sed '3s/$/>/; 4d' $book_xml > $xml_file
ln -s ../$book_id/$lng/media $tmp/

case "$format" in
  html )
     outdir=$output/$book.docbook2/
     mkdir $outdir
     docbook2html -o $outdir $xml_file
     #stylesheet=$stylesheets/html/ldp.dsl#html
     #docbook2html -o $outdir -d $stylesheet $xml_file

     ### create a link to the media files of the book
     ln -s ../../media $outdir
     ;;

  html1 )
     outdir=$output/$book.docbook2/
     mkdir $outdir
     docbook2html -o $outdir --nochunks $xml_file

     ### create a link to the media files of the book
     ln -s ../../media $outdir
     ;;

  rtf )
     docbook2rtf -o $output $xml_file
     #stylesheet=$stylesheets/html/ldp.dsl#print
     #docbook2rtf -o $outdir -d $stylesheet $xml_file
     ;;

  pdf | ps )
     convert=docbook2$format
     $convert -o $output $xml_file
     ;;

  tex | texi | txt )
     convert=docbook2$format
     $convert -o $output $xml_file
     ln -s ../media $output
     ;;

  * ) usage ;;

esac

### clean up
rm -rf $tmp/