<?xml version='1.0'?><!-- -*-SGML-*- -->
<!--
This file  is part of  DocBookWiki.  DocBookWiki is a  web application
that  displays  and  edits  DocBook  documents.  

Copyright (C) 2004, 2005 Dashamir Hoxha, dashohoxha@users.sf.net

DocBookWiki is free software; you can redistribute it and/or modify it
under the terms of the GNU  General Public License as published by the
Free Software Foundation; either version 2 of the License, or (at your
option) any later version.

DocBookWiki is  distributed in  the hope that  it will be  useful, but
WITHOUT   ANY  WARRANTY;   without  even   the  implied   warranty  of
MERCHANTABILITY  or FITNESS  FOR A  PARTICULAR PURPOSE.   See  the GNU
General Public License for more details.

You  should have received  a copy  of the  GNU General  Public License
along with DocBookWiki; if not, write to the Free Software Foundation,
Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
-->

<!-- Creates the table-of-contents of the book or article. -->

<xsl:transform xmlns:xsl="http://www.w3.org/1999/XSL/Transform" version="1.0">

<!-- create the table of contents of the book or article -->
<xsl:template match="book | article" mode="toc">
  <a name="toc" />
  <dl class="toc">
    <xsl:apply-templates mode="toc" />
  </dl>
  <hr />
</xsl:template>

<!-- subnodes for a section -->
<xsl:template  mode="toc"
               match="preface | appendix | bibliography | chapter | section">
  <xsl:variable name="href">
    <xsl:value-of select="concat('#', @id)" />
  </xsl:variable>
  <xsl:variable name="css_class">
    <xsl:apply-templates select="." mode="css_class" />
  </xsl:variable> 
  <xsl:variable name="title">
    <xsl:choose>
      <xsl:when test="name(.)='preface'">
	<xsl:value-of select="concat('Preface: ', ./title)" />
      </xsl:when>
      <xsl:when test="name(.)='bibliography'">
	<xsl:value-of select="concat('Bibliography: ', ./title)" />
      </xsl:when>
      <xsl:when test="name(.)='appendix'">
	<xsl:value-of select="concat('Appendix: ', ./title)" />
      </xsl:when>
      <xsl:otherwise>
	<xsl:variable name="nr">
	  <xsl:number count="chapter | section" from="/" level="multiple" format="1" />
	</xsl:variable>    
	<xsl:value-of select="concat($nr, ' - ', ./title)" />
      </xsl:otherwise>
    </xsl:choose>
  </xsl:variable> 

  <dt class="{$css_class}">
    <a href="{$href}"><xsl:value-of select="$title"/></a>
  </dt>
  <xsl:if test="./section">
    <dd><dl><xsl:apply-templates mode="toc"/></dl></dd>
  </xsl:if>
</xsl:template>

<xsl:template mode="css_class"
              match="preface | chapter | appendix | bibliography">
  <xsl:value-of select="name(.)" />
</xsl:template>

<xsl:template mode="css_class" match="section">
  <xsl:variable name="level">
    <xsl:call-template name="get-section-level" />
  </xsl:variable>
  <xsl:value-of select="concat('sect', $level)" />
</xsl:template>


<!-- ignore text nodes -->
<xsl:template match="text()" mode="toc" />

</xsl:transform>
