<?xml version='1.0'?><!-- -*-SGML-*- -->
<!--
This file  is part of  DocBookWiki.  DocBookWiki is a  web application
that  displays  and  edits  DocBook  documents.  

Copyright (C) 2004, 2005 Dashamir Hoxha, dashohoxha@users.sf.net

DocBookWiki is free software; you can redistribute it and/or modify it
under the terms of the GNU  General Public License as published by the
Free Software Foundation; either version 2 of the License, or (at your
option) any later version.

DocBookWiki is  distributed in  the hope that  it will be  useful, but
WITHOUT   ANY  WARRANTY;   without  even   the  implied   warranty  of
MERCHANTABILITY  or FITNESS  FOR A  PARTICULAR PURPOSE.   See  the GNU
General Public License for more details.

You  should have received  a copy  of the  GNU General  Public License
along with DocBookWiki; if not, write to the Free Software Foundation,
Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
-->

<!-- Used to convert quran.xml to html. -->

<xsl:transform xmlns:xsl="http://www.w3.org/1999/XSL/Transform" version="1.0">

<xsl:output method="xml" version="1.0" encoding="utf-8" 
            omit-xml-declaration="yes" standalone="yes" indent="yes" />

<!-- output the DOCTYPE declaration -->
<xsl:template match="/">
<xsl:text disable-output-escaping="yes"><![CDATA[<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN"
          "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
]]>
</xsl:text>
<xsl:apply-templates />
</xsl:template>

<!-- match book -->
<xsl:template match="book">
<html xmlns="http://www.w3.org/1999/xhtml" lang="EN">
<head>
  <title><xsl:value-of select="./bookinfo/title" /></title>  
  <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
  <link type="text/css" rel="stylesheet" href="quran.css" />
</head>
<body>

  <h1><xsl:value-of select="./bookinfo/title" /></h1>

  <xsl:apply-templates select="./para" />
  <xsl:apply-templates select="./chapter" />

</body>
</html>
</xsl:template>


<!-- match chapter -->
<xsl:template match="chapter">
  <xsl:variable name="chapter-title">
    <xsl:number count="chapter" from="book" level="single" format="1" />
    <xsl:text> - </xsl:text>
    <xsl:value-of select="./title" />
  </xsl:variable>

  <h2><xsl:value-of select="$chapter-title" /></h2>

  <xsl:apply-templates select="./para" />

  <ol>
    <xsl:apply-templates select="./simplesect" />
  </ol>
</xsl:template>


<!-- match simplesect -->
<xsl:template match="simplesect">
  <li><xsl:apply-templates select="./para" /></li>
</xsl:template>


<xsl:template match="para">
  <xsl:value-of select="normalize-space(.)" />
</xsl:template>


</xsl:transform>
