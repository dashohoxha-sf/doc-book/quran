#!/bin/bash
### Convert to other formats using simple XSL transformers.
### It generates directly the formats: latex, text and html1; then
### from the latex format generates also the formats pdf and rtf.

### go to this dir
cd $(dirname $0)

function usage
{
  echo "Usage: ${0} lng"
  echo "where lng can be: en, sq_AL, etc. "
  exit 1
}

if [ "$1" = "" ]; then usage; fi
lng=${1:-en}  #default is english

xml_file=xml_source/quran_$lng.xml


### make the output directory
formats=formats/quran/$lng/
rm -rf $formats
mkdir -p $formats

### the format xml
cp $xml_file $formats/quran_$lng.xml

### generate the format latex
xsltproc -o $formats/quran_$lng.tex xslt-q/xml2latex.xsl $xml_file

### generate the format text
xsltproc -o $formats/quran_$lng.txt xslt-q/xml2text.xsl $xml_file

### generate the format html
xsltproc -o $formats/quran_$lng.html xslt-q/xml2html.xsl $xml_file
cp xslt-q/quran.css $formats/

### generate the format pdf
path=$(pwd)
cd $formats/
pdflatex quran_$lng.tex
rm quran_$lng.aux quran_$lng.log
cd $path

