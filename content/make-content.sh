#!/bin/bash
### creates the content of the folders books/, books_html/,
### workspace/books/, etc. by exploding the initial xml files

### go to this dir
cd $(dirname $0)

### create some directories
mkdir -p books/svn workspace initial_xml/uploaded

### make sure that SVN is initialized
if [ ! -f SVN/svn_cfg.txt ]; then SVN/init.sh; fi

### import docbookwiki_guide/en
./import.sh initial_xml/docbookwiki_guide_en.xml \
            docbookwiki_guide en \
            initial_xml/docbookwiki_guide.media.tgz

### import quran/sq_AL
./import.sh initial_xml/quran_sq_AL.xml
./import.sh initial_xml/quran_sq_AL_2.xml

### import quran/en
#./import.sh initial_xml/quran_en.xml
#./import.sh initial_xml/quran_sq_AL.xml quran en

