#!/bin/bash
### The directory 'content/' has been moved to another
### filesystem (partition) with ReiserFS format, which
### has better performance than ext3 for many small files.

### go to this directory
cd $(dirname $0)

### must be called by root
if [ "$(whoami)" != "root" ]
then
  echo "$0: must be called by root"
  exit 1
fi

### mount the content filesystem
mount --bind /mnt/hda9/books-quran/content/ content/

