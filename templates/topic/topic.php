<?php
  /*
   This file is part of DocBookWiki.  DocBookWiki is a web application
   that displays and edits DocBook documents.

   Copyright (C) 2004, 2005, 2006, 2007
   Dashamir Hoxha, dashohoxha@users.sourceforge.net

   DocBookWiki is free software; you can redistribute it and/or modify
   it under the  terms of the GNU General  Public License as published
   by the Free  Software Foundation; either version 2  of the License,
   or (at your option) any later version.

   DocBookWiki is distributed in the  hope that it will be useful, but
   WITHOUT  ANY  WARRANTY;  without   even  the  implied  warranty  of
   MERCHANTABILITY or  FITNESS FOR A PARTICULAR PURPOSE.   See the GNU
   General Public License for more details.

   You should have  received a copy of the  GNU General Public License
   along  with  DocBookWiki;  if  not,  write  to  the  Free  Software
   Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307
   USA
  */

include_once TPL.'languages/func.languages.php';

class topic extends WebObject
{
  function init()
  {
    $this->addSVar('id', 'Right_Deeds');
    $this->addSVar('title', T_("Right Deeds"));
    $this->addSVar('extracts', UNDEFINED);
    $this->addSVar('format', 'html');  // html | text
    $this->addSVar('lng', 'en');
  }

  function on_set_format($event_args)
  {
    $format = $event_args['format'];
    $this->setSVar('format', $format);
  }

  function on_set_lng($event_args)
  {
    $lng = $event_args['lng'];
    $this->setSVar('lng', $lng);
  }

  function on_extract($event_args)
  {
    $extracts = $event_args['extracts'];
    $extracts = str_replace("\r\n", "\n", $extracts);
    $this->setSVar('extracts', $extracts);
  }

  function get_topic_file($topic_id)
  {
    $dir = TPL.'topic/topics';
    $file = "$dir/$topic_id.txt";
    return $file;
  }

  function on_save($event_args)
  {
    $extracts = $event_args['extracts'];
    $topic_id = $event_args['topic_id'];

    $fname = $this->get_topic_file($topic_id);
    write_file($fname, $extracts);

    $this->setSVar('id', $topic_id);
    $this->setSVar('title', $topic_id);
    $this->setSVar('extracts', UNDEFINED);
  }

  function on_delete($event_args)
  {
    $topic_id = $event_args['topic_id'];

    $fname = $this->get_topic_file($topic_id);
    shell("rm $fname");

    $this->setSVar('id', 'Right_Deeds');
    $this->setSVar('title', 'Right Deeds');
    $this->setSVar('extracts', UNDEFINED);
  }

  function onRender()
  {
    //get state variables
    extract($this->getSVars());

    //extract the extracts
    if ($extracts==UNDEFINED)
      {
        $fname = $this->get_topic_file($id);
        $extracts = implode('', file($fname));
      }
    $quran_verses = $this->extract_verses($extracts, $format);

    //add recordsets topic_langs and formats
    $this->add_recordsets();

    //add template variables
    WebApp::addVars(compact('title', 'extracts', 'quran_verses'));

    //only those who have a username/password can edit the topics
    WebApp::addVar('edit', EDIT);
  }

  /** add recordsets topic_langs and formats */
  function add_recordsets()
  {
    global $webPage;

    //topic_langs
    $rs = new EditableRS('topic_langs');
    $langs = WebApp::getSVar('docbook->languages');
    $arr_langs = explode(',', $langs);
    $arr_lng_details = get_arr_languages();
    for ($i=0; $i < sizeof($arr_langs); $i++)
      {
        $id = $arr_langs[$i];
        $label = $arr_lng_details[$id]['name'];
        $rs->addRec(compact('id', 'label'));
      }
    $webPage->addRecordset($rs);

    //formats
    $rs = new EditableRS('formats');
    $rs->addRec(array('id' => 'html', 'label' => 'html'));
    $rs->addRec(array('id' => 'text', 'label' => 'text'));
    $webPage->addRecordset($rs);

    //topics
    $rs = new EditableRS('topics');
    $rs->addRec(array('id'=>'', 'label'=>''));
    $topics_dir = TPL.'topic/topics/';
    $output = shell("ls $topics_dir | sed /~/d");
    $arr_lines = explode("\n", $output);
    for ($i=0; $i < sizeof($arr_lines); $i++)
      {
        $line = $arr_lines[$i];
        if (trim($line)=='')  continue;
        $topic_id = ereg_replace("\.txt.*", '', $line);
        $rs->addRec(array('id' => $topic_id, 'label' => $topic_id));
      }
    $webPage->addRecordset($rs);
  }

  /**
   * Extract the verses in $extracts and returns them in the given $format.
   * The format of $extracts is this: each chapter is in a separate line,
   * precided by chapter nr and ':', then a comma separated list of verse
   * numbers follows, where each verse number can also be a range; any empty 
   * lines are ignored. For example:
   * 2: 3, 4, 10-13, 17, 24-27
   * 5: 11, 15-17, 20 , 21
   */
  function extract_verses($extracts, $format ='text')
  {
    $func_name = "begin_$format";
    $output = $this->$func_name();

    $arr_lines = explode("\n", $extracts);
    for ($i=0; $i < sizeof($arr_lines); $i++)
      {
        $line = trim($arr_lines[$i]);
        if ($line=='')  continue;
        list($chap_nr, $verse_list) = split(' *: *', $line, 2);

        $chap_title = $this->get_chap_title($chap_nr);
        $func_name = "start_chapter_$format";
        $output .= $this->$func_name($chap_nr, $chap_title);

        $arr_verses = explode(',', $verse_list);
        for ($j=0; $j < sizeof($arr_verses); $j++)
          {
            $range = trim($arr_verses[$j]);
            list($start,$end) = explode('-', $range);
            if (!isset($end))  $end = $start;
            for ($v_nr=$start; $v_nr <= $end; $v_nr++)
              {
                $verse = $this->get_verse($chap_nr, $v_nr);
                $func_name = "verse_to_$format";
                $output .= $this->$func_name($chap_nr, $v_nr, $verse);
              }
          }

        $func_name = "end_chapter_$format";
        $output .= $this->$func_name($chap_nr, $chap_title);
      }

    $func_name = "end_$format";
    $output .= $this->$func_name();

    return $output;
  }

  /** Return the title of the chapter (sura) with the number $s */
  function get_chap_title($s)
  {
    $lng = $this->getSVar('lng');
    $xml_file = BOOKS."quran/$lng/s-$s/content.xml";
    $xsl_file = XSLT."topic/get_chap_title.xsl";
    $title = shell("xsltproc $xsl_file $xml_file");
    return $title;
  }

  /**
   * Return the content of a verse (ayat), given the sura (chapter) number $s
   * and the ayat (verse) number $a.
   */
  function get_verse($s, $a)
  {
    $lng = $this->getSVar('lng');
    $xml_file = BOOKS."quran/$lng/s-$s/a-$s-$a/content.xml";
    $xsl_file = XSLT."topic/get_verse.xsl";
    $verse = shell("xsltproc $xsl_file $xml_file");
    return $verse;
  }


  /*--------------------- text functions -----------------------*/
  
  function begin_text() 
  {
    return "<xmp>";
  }
  
  function end_text()
  {
    return "\n</xmp>";
  }

  function start_chapter_text($chap_nr, $chap_title)
  {
    return "\n$chap_nr. $chap_title\n";
  }

  function end_chapter_text($chap_nr, $chap_title)
  {
    return "\n----------\n";
  }

  function verse_to_text($chap_nr, $v_nr, $verse)
  {
    return "\n$v_nr. $verse\n";
  }


  /*--------------------- html functions -----------------------*/
  
  function begin_html() 
  {
    return "<div class='extracts'>";
  }
  
  function end_html()
  {
    return "\n</div>";
  }

  function start_chapter_html($chap_nr, $chap_title)
  {
    $href="javascript:set_node('./s-$chap_nr/')";
    $link = "<a class=\"button\" href=\"$href\">-&gt;</a>";
    $str = "
    <h3>$chap_nr. $chap_title $link</h3>
    <ol type='1'>
";
    return $str;
  }

  function end_chapter_html($chap_nr, $chap_title)
  {
    $str = "
    </ol>
    <hr width='50%'/>
";
    return $str;
  }

  function verse_to_html($chap_nr, $v_nr, $verse)
  {
    $href="javascript:set_node('./s-$chap_nr/a-$chap_nr-$v_nr/')";
    $link = "<a class=\"button\" href=\"$href\">-&gt;</a>";
    $str = "
      <li value='$v_nr'>$verse $link</li>
";
    return $str;
  }
}
?>