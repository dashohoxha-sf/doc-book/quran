// -*-C-*-
/*
  This file is part of  DocBookWiki.  DocBookWiki is a web application
  that displays and edits DocBook documents.

  Copyright (C) 2004, 2005, 2006, 2007
  Dashamir Hoxha, dashohoxha@users.sourceforge.net

  DocBookWiki is free software;  you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free  Software Foundation; either  version 2 of the  License, or
  (at your option) any later version.

  DocBookWiki is distributed  in the hope that it  will be useful, but
  WITHOUT  ANY   WARRANTY;  without  even  the   implied  warranty  of
  MERCHANTABILITY or  FITNESS FOR A  PARTICULAR PURPOSE.  See  the GNU
  General Public License for more details.

  You should  have received a copy  of the GNU  General Public License
  along  with  DocBookWiki;  if   not,  write  to  the  Free  Software
  Foundation, Inc., 59 Temple  Place, Suite 330, Boston, MA 02111-1307
  USA
*/

function close_topic()
{
  SendEvent('main', 'docbook');
}

function set_topic_lng(listbox)
{
  var idx = listbox.selectedIndex;
  var lng = listbox.options[idx].value;
  SendEvent('topic', 'set_lng', 'lng='+lng);
}

function set_topic_format(listbox)
{
  var idx = listbox.selectedIndex;
  var format = listbox.options[idx].value;
  SendEvent('topic', 'set_format', 'format='+format);
}

function clear_text()
{
  var form = document.topic;
  form.extracts.value = '';
}

function extract()
{
  var form = document.topic;
  var extracts = form.extracts.value;
  
  SendEvent('topic', 'extract', 'extracts='+extracts);
}

function view_topic()
{
  var form = document.topic;
  var topic_id = form.topic_id.value;
  topic(topic_id, topic_id);
}

function save_topic()
{
  var form = document.topic;
  var extracts = form.extracts.value;
  var topic_id = form.topic_id.value;
  var event_args = 'extracts='+extracts+';topic_id='+topic_id;

  SendEvent('topic', 'save', event_args);
}

function del_topic()
{
  var form = document.topic;
  var topic_id = form.topic_id.value;
  var msg = T_("You are deleting the topic 'v_topic_id'.");
  msg = msg.replace(/v_topic_id/, topic_id, msg);

  if (confirm(msg))
    {
      SendEvent('topic', 'delete', 'topic_id='+topic_id);
    }
}

function select_topic(listbox)
{
  //get the selected topic id
  var idx = listbox.selectedIndex;
  var topic_id = listbox.options[idx].value;

  //set it to the field topic_id of the form
  document.topic.topic_id.value = topic_id;
}

function set_node(node_path)
{
  var lng = session.getVar('topic->lng');
  var event_args = 'book_id=quran;node_path='+node_path+';lng='+lng;
  SendEvent('main', 'docbook', event_args);
}
