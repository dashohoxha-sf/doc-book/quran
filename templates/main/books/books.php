<?php
  /*
   This file is part of DocBookWiki.  DocBookWiki is a web application
   that displays and edits DocBook documents.

   Copyright (C) 2004, 2005, 2006, 2007
   Dashamir Hoxha, dashohoxha@users.sourceforge.net

   DocBookWiki is free software; you can redistribute it and/or modify
   it under the  terms of the GNU General  Public License as published
   by the Free  Software Foundation; either version 2  of the License,
   or (at your option) any later version.

   DocBookWiki is distributed in the  hope that it will be useful, but
   WITHOUT  ANY  WARRANTY;  without   even  the  implied  warranty  of
   MERCHANTABILITY or  FITNESS FOR A PARTICULAR PURPOSE.   See the GNU
   General Public License for more details.

   You should have  received a copy of the  GNU General Public License
   along  with  DocBookWiki;  if  not,  write  to  the  Free  Software
   Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307
   USA
  */

  /**
   * A listbox of the books/articles in the system.
   */
class books extends WebObject
{
  function onRender()
  {
    //add the recordset 'books'
    $rs = new EditableRS("books");
    $book_list = CONTENT.'books/book_list';
    $books = shell("cat $book_list | cut -d: -f1 | uniq");
    $arr_books = explode("\n", trim($books));
    for ($i=0; $i < sizeof($arr_books); $i++)
      {
	$book_id = trim($arr_books[$i]);
        $rs->addRec(array('id'=>$book_id, 'title'=>$book_id));
      }
    global $webPage;
    $webPage->addRecordset($rs);

    //add variable show_books_list
    $show = (sizeof($arr_books) > 2 ? 'true' : 'false');
    WebApp::addVar('show_books_list', $show);
  }
}
?>
