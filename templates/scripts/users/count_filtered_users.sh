#!/bin/bash
### get the number of the filtered users

### get the parameter
filter="$1"

### filter the users using the given filter, and count them
users=templates/admin/access_rights/users
gawk -F: "$filter" $users | wc -l
