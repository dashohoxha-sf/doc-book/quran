#!/bin/bash
### get the filtered users in the given range

### get the parameters
filter=$1
first=$2
last=$3

### filter the users according to the given filter
### sort them, and return only the results in the given range
query="$filter {print \$1}"
users=templates/admin/access_rights/users
gawk -F: "$query" $users | sort | sed -n "$first,${last}p"
