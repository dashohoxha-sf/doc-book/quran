#!/bin/bash
### returns the access rights file for the given user in the given book

### get the parameters book_id and username
book_id=$1
username=$2

### print the content of the access rights file of the user
book_access_rights=templates/admin/access_rights/$book_id
cat $book_access_rights/$username
