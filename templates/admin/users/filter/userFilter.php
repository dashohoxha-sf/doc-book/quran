<?php
  /*
   This file is part of DocBookWiki.  DocBookWiki is a web application
   that displays and edits DocBook documents.

   Copyright (C) 2004, 2005, 2006, 2007
   Dashamir Hoxha, dashohoxha@users.sourceforge.net

   DocBookWiki is free software; you can redistribute it and/or modify
   it under the  terms of the GNU General  Public License as published
   by the Free  Software Foundation; either version 2  of the License,
   or (at your option) any later version.

   DocBookWiki is distributed in the  hope that it will be useful, but
   WITHOUT  ANY  WARRANTY;  without   even  the  implied  warranty  of
   MERCHANTABILITY or  FITNESS FOR A PARTICULAR PURPOSE.   See the GNU
   General Public License for more details.

   You should have  received a copy of the  GNU General Public License
   along  with  DocBookWiki;  if  not,  write  to  the  Free  Software
   Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307
   USA
  */

  /**
   * userFilter is used to select the users that are displayed in the list.
   * Users are those that can login at edit.php and admin.php interfaces
   * (they could have been called also editors). Only the superuser can 
   * access the users module. 
   *
   * @package admin
   * @subpackage users
   */
class userFilter extends WebObject
{
  function init()
  {
    $this->addSVars( array(
                           'username' => '',
                           'realname' => '',
                           'email' => '',
                           'books' => '',
                           'level' => '',
                           'nodes' => '',
                           'langs' => '',
                           'filter' => '1'
                           ) );
  }

  function onParse()
  {
    $this->buildFilter();
  }

  function buildFilter()
  {
    //get state vars
    extract($this->getSVars());

    $arr_filters = array();
    if ($username != '')
      {
        $arr_filters[] = '($1 ~ "'.$username.'")';
      }

    if ($realname != '')
      {
        $arr_filters[] = '($3 ~ "'.$realname.'")';
      }

    if ($email != '')
      {
        $arr_filters[] = '($4 ~ "'.$email.'")';
      }

    if ($books != '')
      {
        $arr_filters[] = '($5 ~ "'.$books.'")';
      }

    $filter = implode(' && ', $arr_filters);

    if ($filter=='')  $filter = '1';
    $filter = '('.$filter.')';
    $filter = str_replace('"', '\\"', $filter);
    $filter = str_replace('$', '\\$', $filter);

    $this->setSVar('filter', $filter);
  }  
}
?>