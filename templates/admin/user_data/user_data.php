<?php
  /*
   This file is part of DocBookWiki.  DocBookWiki is a web application
   that displays and edits DocBook documents.

   Copyright (C) 2004, 2005, 2006, 2007
   Dashamir Hoxha, dashohoxha@users.sourceforge.net

   DocBookWiki is free software; you can redistribute it and/or modify
   it under the  terms of the GNU General  Public License as published
   by the Free  Software Foundation; either version 2  of the License,
   or (at your option) any later version.

   DocBookWiki is distributed in the  hope that it will be useful, but
   WITHOUT  ANY  WARRANTY;  without   even  the  implied  warranty  of
   MERCHANTABILITY or  FITNESS FOR A PARTICULAR PURPOSE.   See the GNU
   General Public License for more details.

   You should have  received a copy of the  GNU General Public License
   along  with  DocBookWiki;  if  not,  write  to  the  Free  Software
   Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307
   USA
  */

include_once SCRIPTS.'user_data.php';

/**
 * The user can change his data: name, email and password; he can also
 * see which books he can manage, his edit rights and a list of nodes
 * that are modified and he can approve.
 *
 * @package admin
 * @subpackage user_data
 */
class user_data extends WebObject
{
  function on_save($event_args)
  {
    $user_data = get_user_data(USER);
    $user_data['name'] = $event_args['name'];
    $user_data['email'] = $event_args['email'];
    save_user_data($user_data);
  }

  function on_change_passwd($event_args)
  {
    $new_password = $event_args['new_password'];
    $user_data = get_user_data(USER);

    //encrypt the new password and save it
    srand(time());
    $user_data['password'] = crypt($new_password, rand());
    save_user_data($user_data);
    WebApp::message(T_("Password changed successfully."));
  }

  function onParse()
  {
    WebApp::setSVar('edit_rights->user', USER);
  }

  function onRender()
  {
    $user_data = get_user_data(USER);
    WebApp::addVar('name', $user_data['name']);
    WebApp::addVar('email', $user_data['email']);

    $this->add_rs_books_admin();
  }

  /**
   * Add to webPage a recordset of the books for which the user is admin.
   */
  function add_rs_books_admin()
  {
    $rs = new EditableRS('books_admin');

    $user_data = get_user_data(USER);
    $arr_books = explode(',', $user_data['books']);

    for ($i=0; $i < sizeof($arr_books); $i++)
      {
        $book_id = trim($arr_books[$i]);
        if ($book_id=='')  continue;
        $rs->addRec(compact('book_id'));
      }

    global $webPage;
    $webPage->addRecordset($rs);
  }
}
?>
