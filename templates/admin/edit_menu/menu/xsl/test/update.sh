#!/bin/bash

id=nontechnical
topicid=non-tech
caption='Non Technical'
#xsl_file=../update_topicid.xsl
xsl_file=../update_caption.xsl
xml_file=../../menu_en.xml

xsltproc  --stringparam id "$id" \
          --stringparam topicid "$topicid" \
          --stringparam caption "$caption" \
          $xsl_file  $xml_file
