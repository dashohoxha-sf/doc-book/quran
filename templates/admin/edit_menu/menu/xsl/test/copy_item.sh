#!/bin/bash

id=technical
#copy_id=category_4
copy_id=nontechnical
xsl_file=../copy_item.xsl
xml_file=../../menu_en.xml

xsltproc  --stringparam id "$id" \
          --stringparam copy_id "$copy_id" \
          $xsl_file  $xml_file
