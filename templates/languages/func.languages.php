<?php
  /*
   This file is part of DocBookWiki.  DocBookWiki is a web application
   that displays and edits DocBook documents.

   Copyright (C) 2004, 2005, 2006, 2007
   Dashamir Hoxha, dashohoxha@users.sourceforge.net

   DocBookWiki is free software; you can redistribute it and/or modify
   it under the  terms of the GNU General  Public License as published
   by the Free  Software Foundation; either version 2  of the License,
   or (at your option) any later version.

   DocBookWiki is distributed in the  hope that it will be useful, but
   WITHOUT  ANY  WARRANTY;  without   even  the  implied  warranty  of
   MERCHANTABILITY or  FITNESS FOR A PARTICULAR PURPOSE.   See the GNU
   General Public License for more details.

   You should have  received a copy of the  GNU General Public License
   along  with  DocBookWiki;  if  not,  write  to  the  Free  Software
   Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307
   USA
  */

  /**
   * This function returns an associated array of language codes
   * and language details (like language name, codeset, etc.).
   * It can be used like this:
   *   $langs = get_arr_languages();
   *   $name = $langs['en']['name'];
   *   $codeset = $langs['en']['codeset'];
   */
function get_arr_languages()
{
  $langs = 
    array(
          'sq_AL' =>
          array('name'    => 'Albanian', 
                'codeset' => 'iso-8859-1'),

          'sq_AL_2' =>
          array('name'    => 'Albanian 2', 
                'codeset' => 'iso-8859-1'),

          'en' => 
          array('name'    => 'English', 
                'codeset' => 'iso-8859-1')
          );

  return $langs;
}
?>