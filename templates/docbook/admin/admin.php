<?php
  /*
   This file is part of DocBookWiki.  DocBookWiki is a web application
   that displays and edits DocBook documents.

   Copyright (C) 2004, 2005, 2006, 2007
   Dashamir Hoxha, dashohoxha@users.sourceforge.net

   DocBookWiki is free software; you can redistribute it and/or modify
   it under the  terms of the GNU General  Public License as published
   by the Free  Software Foundation; either version 2  of the License,
   or (at your option) any later version.

   DocBookWiki is distributed in the  hope that it will be useful, but
   WITHOUT  ANY  WARRANTY;  without   even  the  implied  warranty  of
   MERCHANTABILITY or  FITNESS FOR A PARTICULAR PURPOSE.   See the GNU
   General Public License for more details.

   You should have  received a copy of the  GNU General Public License
   along  with  DocBookWiki;  if  not,  write  to  the  Free  Software
   Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307
   USA
  */

  /**
   * Some administration tasks about the book.
   * Can be accessed only by an admin of the book.
   *
   * @package docbook
   * @subpackage admin
   */
class admin extends WebObject
{
  function on_update_search_index($event_args)
  {
    //running the script may take a long time
    set_time_limit(0);

    //get the current book parameters
    $book_id = WebApp::getSVar('docbook->book_id');
    $lng = WebApp::getSVar('docbook->lng');

    //open the html document and print a title
    $title = T_("Updating Search Indexes");
    print "<html><title>$title</title><body><h3>$title</h3><xmp>\n";
    flush();

    //run the script and send the output
    $data_owner = DATA_OWNER;
    passthru("sudo -u $data_owner search/make_index.sh 2>&1");

    //close the html document
    print "\n</xmp></body></html>";

    //stop proccessing
    exit;
  }

  function on_update_downloadables($event_args)
  {
    //running the script may take a long time
    set_time_limit(0);

    //get the current book parameters
    $book_id = WebApp::getSVar('docbook->book_id');
    $lng = WebApp::getSVar('docbook->lng');

    //open the html document and print a title
    $title = T_("Generating Downloadables For v_book_lng");
    $title = str_replace('v_book_lng', "'$book_id/$lng'", $title);
    print "<html><title>$title</title><body><h3>$title</h3><xmp>\n";
    flush();

    //run the script and send the output
    $data_owner = DATA_OWNER;
    $update_downloads_sh = 'content/downloads/update_downloads.sh';
    passthru("sudo -u $data_owner $update_downloads_sh $book_id $lng 2>&1");

    //close the html document
    print "\n</xmp></body></html>";

    //stop proccessing
    exit;
  }

  function on_update_booklist($event_args)
  {
    shell("content/book_list.sh");
  }

  function onRender()
  {
    $this->add_rs_modified_nodes();
  }

  /** add to webPage the recordset modified_nodes */
  function add_rs_modified_nodes()
  {
    $rs = new EditableRS('modified_nodes');

    $book_id = WebApp::getSVar('docbook->book_id');
    $lng = WebApp::getSVar('docbook->lng');
    $filename = WS_BOOKS."$book_id/$lng/modified_nodes.txt";

    if (file_exists($filename))
      {
        $arr_modified_nodes = file($filename);
        for ($i=0; $i < sizeof($arr_modified_nodes); $i++)
          {
            $node_path = $arr_modified_nodes[$i];
            $node_path = trim($node_path);
            $node_title = $this->get_title($book_id, $lng, $node_path);
            $rec = compact('node_path', 'node_title');
            $rs->addRec($rec);          
          }
      }

    global $webPage;
    $webPage->addRecordset($rs);
  }

  function get_title($book_id, $lng, $node_path)
  {
    $cache_path = WS_CACHE."$book_id/$lng/";
    $navigation_file = $cache_path.$node_path."navigation.txt";
    $line = shell("grep full_title $navigation_file");
    $arr = split('=', chop($line), 2);
    $title = $arr[1];
    if ($title=='')  $title = 'Table Of Contents';

    if (ereg('([^/]+) / ([^/]+) /$', $title, $regs))
      $title = "($regs[1], $regs[2])";

    return $title;
  }
}
?>
