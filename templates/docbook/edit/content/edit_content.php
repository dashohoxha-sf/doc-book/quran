<?php
  /*
   This file is part of DocBookWiki.  DocBookWiki is a web application
   that displays and edits DocBook documents.

   Copyright (C) 2004, 2005, 2006, 2007
   Dashamir Hoxha, dashohoxha@users.sourceforge.net

   DocBookWiki is free software; you can redistribute it and/or modify
   it under the  terms of the GNU General  Public License as published
   by the Free  Software Foundation; either version 2  of the License,
   or (at your option) any later version.

   DocBookWiki is distributed in the  hope that it will be useful, but
   WITHOUT  ANY  WARRANTY;  without   even  the  implied  warranty  of
   MERCHANTABILITY or  FITNESS FOR A PARTICULAR PURPOSE.   See the GNU
   General Public License for more details.

   You should have  received a copy of the  GNU General Public License
   along  with  DocBookWiki;  if  not,  write  to  the  Free  Software
   Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307
   USA
  */

include_once dirname(__FILE__).'/convert_from_xml.php';
include_once dirname(__FILE__).'/convert_to_xml.php';
include_once dirname(__FILE__).'/process_content.php';
include_once TPL.'common/funcs/validate.php';

/**
 * @package docbook
 * @subpackage edit
 */
class edit_content extends WebObject
{
  function init()
  {
    $this->addSVar('mode', 'text');  //text | xml | html | latex | texi
    $this->addSVar('normalize', 'false');  //normalize space
  }

  function on_set_mode($event_args)
  {
    $mode = $event_args['mode'];
    $this->setSVar('mode', $mode);
  }

  function on_save($event_args)
  {
    //make sure that the node is not locked by somebody else
    if (locked_by_somebody())  return;

    //get variables
    $content = $event_args['content'];
    $mode = $this->getSVar('mode');

    //replace comments and cdata by tags
    $content = strip_comments($content);
    if ($mode=='text')  $content = expand_cdata($content);
    $content = strip_cdata($content);

    //replace the xml entities
    $content = preg_replace('#&(\w+);#', '&amp;$1;', $content);

    //convert it to xml(docbook)
    $converter = $mode.'_to_xml';
    $xml_content = $converter($content);

    if ($xml_content=='ERROR')
      {
        $this->error_on_save($content);
        return;
      }

    //put back the cdata and the comments
    $xml_content = putback_cdata($xml_content);
    $xml_content = putback_comments($xml_content);

    //validate the xml content
    if (! validate_xml($xml_content))
      {
        $this->error_on_save($content);
        return;
      }

    //write the new content to the content file
    $xml_file = file_content_xml();
    write_file($xml_file, $xml_content);

    //update the cache files content.html
    update_cache();

    //if the node type is simplesect, then update subnodes.html of the parent
    $node_type = WebApp::getVar('node_type');
    if ($node_type=='simplesect')
      {
        $lng = WebApp::getSVar('docbook->lng');
        $node_path = WebApp::getSVar('docbook->node_path');
        //get node path of the parent
        $p_node_path = ereg_replace('[^/]+/$', '', $node_path);
        //update subnodes.html of the parent
        update_subnodes_html($p_node_path, $lng, 'non-recursive');
      }

    //add this node in the list of the modified nodes
    add_to_modified_nodes();

    //set the status of the node to modified
    set_node_status('modified');
  }

  function error_on_save($content)
  {
    $this->error = true;

    $content = putback_cdata($content, 'html');

    $mode = $this->getSVar('mode');
    if ($mode=='text')  $content = compact_cdata($content);

    $content = putback_comments($content);

    WebApp::addGlobalVar('node_content', $content);
    WebApp::message(T_("There was an error, failed to save!"));
  }

  function onRender()
  {
    $this->add_tab_items();
    $mode = $this->getSVar('mode');
    if (!$this->error)
      {
        $node_content = get_node_content(file_content_xml(), $mode);
        WebApp::addVar("node_content", $node_content);
      }
  }

  /**
   * Add to the webpage a recordset with the items of the tabs (modes).
   */
  function add_tab_items()
  {
    $items = array(
                   'text'  => 'Text',
                   'xml'   => 'DocBook',
                   'html'  => 'HTML',
                   'latex' => 'Latex',
                   'texi'  => 'Texi'
                   );

    $rs = new EditableRS('items');

    //fill the recordset
    $mode = $this->getSVar('mode');
    while ( list($item, $label) = each($items) )
      {
        $css_class = ($item==$mode ? 'item-selected' : 'item');
        $rec = array(
                     'item'  => $item, 
                     'label' => $label,
                     'class' => $css_class
                     );
        $rs->addRec($rec);
      }

    //set the recordset to the page
    global $webPage;
    $webPage->addRecordset($rs);
  }
}
?>
